import graphene
from graphene_django import DjangoObjectType
from graphene_django.views import GraphQLView
from graphene_django.forms.mutation import DjangoModelFormMutation
from graphene import ID , Field, ObjectType, Schema, relay,Context
# from graphene import Context
from graphene_file_upload.scalars import Upload
from django.conf import settings
from django.core.files.storage import FileSystemStorage
import graphql_jwt
from graphql_jwt.decorators import staff_member_required,login_required
# from graphql_jwt.decorators import login_required
# import django_filters
# from graphene_django.filter import DjangoFilterConnectionField
from django.views.decorators.csrf import ensure_csrf_cookie
from user.models import User

class UserType(DjangoObjectType):
    class Meta:
        model = User
        exclude = ('password',)

class CreateUser(graphene.Mutation):
    
    user = graphene.Field(UserType)
    class Arguments:
        password = graphene.String(required=True)
        email = graphene.String(required=True)

    def mutate(self, info, password, email):
        
        user=User.objects._create_user( 
            email,
            password
        )
        
        user.save()
        return CreateUser(user=user)

class DeleteuserMutation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()
    user=graphene.Field(UserType)

    def mutate(self,info,user,id):
        user=user.objects.get(pk=id)
        user.delete()
        return DeleteuserMutation(user=user)


class UpdateMutation(graphene.Mutation):
    class Arguments:
        email = graphene.String(required=False)
        first_name= graphene.String(required=False)
        is_staff = graphene.Boolean(required=False)
        avatar = graphene.String(required=False)
        last_name = graphene.String(required=False)
        id = graphene.ID()
    
    user=graphene.Field(UserType)
    
    # @login_required
    def mutate(self, info, email, is_staff, first_name, last_name, avatar, id):
        user = info.context.user or None
        print(user)
        if not user.is_authenticated:
             raise Exception('Authentication credentials were not provided')

        if info.context.FILES:
            print(info.context.FILES['avatar'])
            image=info.context.FILES['avatar']
            fs = FileSystemStorage()
            avatar= fs.save(image.name, image)
            # print(avatar)
            # print(fs.url(avatar))
        user= User.objects.get(pk=id)
        user.email=email
        user.is_staff=is_staff
        user.first_name=first_name
        user.avatar=avatar
        user.last_name=last_name
        user.save()
        return UpdateMutation(user=user)

class Mutation(graphene.ObjectType):
    create_user=CreateUser.Field()
    delete_user=DeleteuserMutation.Field()
    update_user=UpdateMutation.Field()


class Query(graphene.ObjectType):
    user = graphene.Field(UserType,
                            id=graphene.Int(),
                            email=graphene.String(),
                            is_staff=graphene.Boolean(),
                            first_name=graphene.String()
                            )

    all_users = graphene.List(UserType)
    me = graphene.Field(UserType)
    
    def resolve_me(self, info):
        user = info.context.user
        print(user)
        if user.is_anonymous:
            raise Exception('Not logged in!')
        return user

    # @login_required
    def resolve_all_users(self, info, **kwargs):
        
        return User.objects.all()

    def resolve_user(self,info,**kwargs):
        print("user:")
        print(info.context.user)

        id = kwargs.get('id')
        return User.objects.get(pk=id)

    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()
    revoke_token = graphql_jwt.Revoke.Field()
    delete_token_cookie = graphql_jwt.DeleteJSONWebTokenCookie.Field()
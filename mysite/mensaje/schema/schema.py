# from django.contrib.auth import get_user_model
import graphene
from graphene_django import DjangoObjectType
from graphene_django.views import GraphQLView
from graphene_django.forms.mutation import DjangoModelFormMutation
from graphene import ID , Field, ObjectType, Schema, relay,Context
# from graphene import Context
from graphene_file_upload.scalars import Upload
from django.conf import settings
from django.core.files.storage import FileSystemStorage
import graphql_jwt
from graphql_jwt.decorators import staff_member_required,login_required
# from graphql_jwt.decorators import login_required
import django_filters
from graphene_django.filter import DjangoFilterConnectionField
from django.views.decorators.csrf import ensure_csrf_cookie
from django.utils import timezone

from datetime import datetime
from mensaje.models import Mensaje
from django_filters import FilterSet, OrderingFilter
class MensajeNode(DjangoObjectType):
    class Meta:
        
        model = Mensaje
        filter_fields = ['created_date', 'text', 'author']
        interfaces = (relay.Node, )
        # filterset_class = MensajeFilter

class MensajeFilter(django_filters.FilterSet):
    text = django_filters.CharFilter(
        # lookup_choices=[
        # ('exact', 'Equals'),
        # ]
    ) 
    created_date = django_filters.CharFilter(
        # lookup_choices=[
        # ('exact', 'Equals'),
        # ]
    )        

    # order_by = OrderingFilter(
    #     fields=(
    #         ('created_date', 'created_date'),
    #     )
    # )

    class Meta:
        model = Mensaje
        fields = [ 'text','created_date']



class MensajeType(DjangoObjectType):
    class Meta:
        model=Mensaje
    

class CreateMensaje(graphene.Mutation):
    mensaje= graphene.Field(MensajeType)

    class Input:
        title=graphene.String()
        text=graphene.String()        
        

    def mutate(self, info, **input):
        
        user=info.context.user or None
        if user.is_anonymous:
            raise Exception('Not logged in!')
        print(user)
        mensaje=Mensaje(
            title=input.get('title'),
            text=input.get('text'),
            author=user,
        )
        mensaje.save()
        return CreateMensaje(mensaje=mensaje)


class DeleteMensaje(graphene.Mutation):
    mensaje=graphene.Field(MensajeType)
    class Arguments:
        pk= graphene.ID()
    def mutate(self,info,pk):
        mensaje=Mensaje.objects.get(pk=pk)
        mensaje.delete()
        return DeleteMensaje(mensaje=mensaje)


class Query(graphene.ObjectType):
    all_mensajes = DjangoFilterConnectionField(MensajeNode)

    all_mensajesf = DjangoFilterConnectionField(MensajeNode,
                                              filterset_class=MensajeFilter)


    mensaje=graphene.List(MensajeType,
                            # id=graphene.ID(),
                            created_date=graphene.String()
                            )
    mensajes=graphene.List(MensajeType)

    

    # def resolve_mensaje(self,info, **kwargs):
    #     id=kwargs.get('id')
    #     # created_date=datetime.strptime(kwargs.get('created_date'))
    #     created_date=kwargs.get('created_date')
    #     return Mensaje.objects.select_related('author').get(created_date=created_date)
    
    def resolve_mensaje(self,info, **kwargs):
        created_date=kwargs.get('created_date')
        return Mensaje.objects.select_related('author').filter(created_date=  created_date)

    def resolve_mensajes(self, info):
        return Mensaje.objects.select_related('author').all()


class Mutation(graphene.ObjectType):
    create_mensaje=CreateMensaje.Field()
    delete_mensaje=DeleteMensaje.Field()



from django.db import models
from django.utils import timezone
from user.models import User
# Create your models here.
class Mensaje(models.Model):

    title=models.CharField(max_length=100)
    text= models.CharField(max_length=400)
    created_date=models.DateTimeField(default=timezone.now)

    author=models.ForeignKey('user.User',
            on_delete=models.CASCADE,
            related_name='author'
            )

    def __str__ (self):
        return self.title

from django.urls import path

from . import views
# from user.api.viewsets import userviewsets
# from rest_framework.authtoken import views
from graphene_django.views import GraphQLView
from graphql_jwt.decorators import jwt_cookie
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    path('', views.signup, name='signup'),
    path('login', jwt_cookie(views.signin), name='signin'),
    path('loggoutt', views.loggoutt, name='loggoutt'),
    # path('crud', views.signup, name='crud'),
    path('welcome', jwt_cookie(views.welcome), name="welcome"),
    path('<int:pk>/delete/', jwt_cookie(views.deleteUser), name="delete"),
    path('<int:pk>/edit/',jwt_cookie( views.updateUser), name="edit"),
    path('mensajes',jwt_cookie(views.mensajes), name="mensajes"),
    path('crear_mensaje', jwt_cookie(views.crear_mensaje), name="crear_mensaje"),
    
    # path('graphql/', csrf_exempt(GraphQLView.as_view())),
    # path('api-token-auth/', views.obtain_auth_token, name="auth"),
    # path('api-token-auth/', views.obtain_auth_token, name='api-tokn-auth'), 
]

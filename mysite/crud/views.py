from django.shortcuts import render
from django.contrib.auth.models import User
# from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.contrib import messages
from django.shortcuts import redirect
from django.contrib.auth import logout as logout_user
from django.contrib.auth import login as login_user
from crud.forms import SignUpForm, UsersForm, UpdateForm, SingInForm, MensajeForm
from .models import User
from crud import templates
from django.template import RequestContext, Template
from mysite.schema import schema
import json 
from django.contrib.auth import authenticate
from django.http import HttpResponse
from graphql_jwt.decorators import login_required
from django.test import RequestFactory, TestCase
from graphene.test import Client
from unittest.mock import Mock, patch
from django.views.decorators.csrf import csrf_protect
from graphene_django.views import GraphQLView
import requests
# from rest_framework.authtoken.models import Token
# from rest_framework.response import Response
# from graphql_jwt.refresh_token.signals import refresh_token_rotated

# @receiver(refresh_token_rotated)
# def revoke_refresh_token(sender, request, refresh_token, **kwargs):
#     refresh_token.revoke(request)

# from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
# from rest_framework.permissions import IsAuthenticated
# from rest_framework.views import APIView
# user = authenticate(username='john', password='secret')
# if user is not None:
#     pass

# def signup(request): #new
#     form = SignUpForm()
#     if request.method == 'POST':
#         form = SignUpForm(request.POST)
#         if form.is_valid():
#             user = User.objects.create_user(
#                 email=form.cleaned_data['email'],
#             )
#             user.set_password(form.cleaned_data['password'])
#             user.save()
#             login_user(request, user)
#             return redirect('welcome')
#     context = RequestContext(request,{
#         'form': form
#     })
    
#     return render(request, 'signup.html', {
#         'form': form
#     })

def crear_mensaje(request):
    if request.method=='POST':
        print("POST!!  ")
        form=MensajeForm(request.POST)
        if form.is_valid():
            title=form.cleaned_data['title']
            text=form.cleaned_data['text']
            receiver=form.cleaned_data['receiver']
            result=schema.execute(
                '''
                mutation($title:String!,$text:String!,$receiver:Int){
                relayCreateMensaje(input: {
                    title:$title,
                    text:$text,
                    receiver:$receiver}
                ){
                    mensaje{
                    title
                    text
                    receiver
                    author{
                        id 
                        email
                    }
                    }
                }
                }
            ''',variables={
                "title":title,
                "text":text,
                "receiver":receiver
            },
            context=request
            )
            print(result.data)
            print(result.errors)

            context={
                'form':form
                }
            return render(request, 'mensajes.html', context)
    else:
        print(request.user)
        print("Not POST")
        form=MensajeForm()
        context={
            'form':form
            }
        return render(request, 'mensajes.html', context)
        

def mensajes(request):
    result=schema.execute('''
        query{
        relayMensajes{
            edges{
                node{
                    id
                    title
                    text
                    author{
                    email
                    }
                        }
                    }
                }
            
            }
    ''')
    print(result.data)
    print(result.errors)
    data=result.data['relayMensajes']['edges']
    # data=result.data['relayMensajes']
    # print(data)
    # newdata=data.items()
    # print(newdata['edges'])
    # data=json.dumps(result.data['relayMensajes']['edges'])
    # print(data)
    # data=json.dumps(data)
    context={
        # 'form':form,
        'data':data
        }
    return render(request, 'ver_mensajes.html', context)


def deleteToken(request):
    pass


def loggoutt(request):
    cookie=request.COOKIES.get('JWT')
    # print(request.headers)
    # logout(request)

    # if request.user.is_authenticated:
    #     print("uthenticated")
    # else:
    #     print("not auntenticatefd")

    result=schema.execute('''
        mutation RevokeToken($token:String!) {
        revokeToken(refreshToken: $token) {
            revoked
        }
        }
    ''',
    variables={
        "token":cookie
    }
    )
    print(result.data)
    print(result.errors)
    form=SingInForm()
    context={
        'form':  form,
    }
    return render(request, 'signin.html', context)
    
# @csrf_protect
def signin(request):
    print(request.META)
    print(request.COOKIES)
    if request.method=='POST':
        # headers={}
        # data={}
        # req=requests.post('graphql/', json={

        # })
        query_me=schema.execute('''
            query{
                me{
                    id
                    email
                }
                }'''
            ,
            context_value=request
        )

        print("usuario:")
        print(query_me.data)
        print(query_me.errors)

        username=request.POST['email']
        password=request.POST['password']
        # print(username)
        # username = request.POST.get('email')
        # password = request.POST.get('password')
        # username= request.GET['email']
        # password= request.GET['password']
        # print(username)
        # print(request.user)
        # user = authenticate(email=username, password=password)
        # print(user)
        
        # if not user.is_authenticated:
        #     print("not autenticated")
        # else:
        #     print("auntenticated")

        result=schema.execute('''
            mutation TokenAuth($email: String!, $password: String!) {
            tokenAuth(email: $email, password: $password) {
                token
                payload
                refreshToken
                refreshExpiresIn
            }
            }
        '''
        , variables={
                "email":username ,
                "password":password
                },
            context=request
        )
        print("result")
        print(result.data)
        print(result.errors)
        if result.errors is None:
            #**********token
            refresh_token=result.data['tokenAuth']['refreshToken']
            token=result.data['tokenAuth']['token']
            
            result=schema.execute('''
            mutation VerifyToken($token: String!) {
                verifyToken(token: $token) {
                    payload
                }
                }
            '''
            , variables={
                "token":token
            },
            context=request
        
            )
            print(result.data)
            print(result.errors)
        # req = RequestFactory().get('/')
        # req.user = AnonymousUser()
        # client = Client(schema)
        # client.user = AnonymousUser()
            if result.errors is None:
                result=schema.execute('''
                    mutation refreshToken($refreshToken:String!){
                        refreshToken(refreshToken:$refreshToken){
                            token
                            payload
                            refreshToken
                            refreshExpiresIn
                    }  
                    }
                ''',
                variables={
                    "refreshToken":refresh_token
                }
                )
                print(result.errors)
                print(result.data)
        query_me=schema.execute('''
            query{
                me{
                    id
                    email
                }
                }'''
            ,context_value=request
        )
        print("usuario:")
        print(query_me.data)
        print(query_me.errors)

        form=SingInForm()
        context={
            'form':  form,
            
        }
        # print(result.errors)
        # print(result.data)
        return render(request, 'signin.html', context)
    else:
        # print(request.headers)
        # print(request.COOKIES)
        
        query_me=schema.execute('''
            query{
                me{
                    id
                    email
                }
                }'''
            ,
            context_value=request
        )

        print("usuario:")
        print(query_me.data)
        print(query_me.errors)
        print(request.META)
        print(request.COOKIES)

        form=SingInForm()
        context={
            'form':  form
        }  
        content_type='application/json'
        return render(request, 'signin.html', context )


def signup(request):
    # print(request.headers)
    form = SignUpForm()
    if request.method == 'POST':
        form=SignUpForm(request.POST)
        if form.is_valid():
            email=form.cleaned_data['email']
            #password=form.cleaned_data['password']
            password=request.POST['password']
            create_string='''
                mutation{{
                createUser(email:"{email}", password:"{password}"){{
                    user{{
                        email:email
                        password:password
                    }}
                }}
                }}
            '''.format(email=email, password=password)

            # print(create_string)
            result=schema.execute(create_string)
            print(result.data)
            context={
                'data':result.data,
                'succes':"usuario creado?",
                'form':form
            }
            

            return render(request, 'signup.html', context)
        else:
            context={
                            'succes':"",
                            'form':form,
                            'error':"no creado"
                        }
            return render(request, 'signup.html', context)
    else:
        form=SignUpForm()
        context={
                'succes':"",
                'form':form,
            }
        
        return render(request, 'signup.html',context)


def logout(request):
    
    logout_user(request)
    # print(request.headers)
    return redirect('/')

def welcome(request):

    # print(request.headers)
    form=UsersForm()
    cookie=request.COOKIES.get('JWT')
    quer_string='''
        query GetUsers($token:String!){
        allUsers (token:$token) {
            id
            firstName
            lastName
            email
            isStaff
            isSuperuser
            avatar
        }
        }
    '''
    result=schema.execute(quer_string,
    variables={
        "token":"asdasdas"
    },
    context=request,
    )
    print(result.data)
    print(result.errors)
    context = {
        'data':result.data['allUsers'],
    }
    #return render(request,'profile.html',{ 'form': form})
    return render(request,'profile.html',context)



def deleteUser(request, pk):
    delete_string='''
            mutation{{
            deleteUser(id:{id}){{
            user {{
                email
            }}
            }}
            }}
    '''.format(id=pk)
    result=schema.execute(delete_string)
    return redirect('welcome')

# @login_required

# @api_view(['GET'])
# @authentication_classes([SessionAuthentication, BasicAuthentication])
# @permission_classes([IsAuthenticated])
def updateUser(request, pk):
    # print(request.headers)
    if request.user.is_authenticated:
            print("authenticated")
    else:
        print("not auntenticatefd")
            # username = request.user.email
            # password = request.user.password
            # print(request.auth)
            # user = authenticate(username='lemon@gmail.com', password='123')
            # # print(user)
            # # print(request.user)
            # token = Token.objects.get_or_create(user=user)
            # token= Token.objects.get(user=user)
            # print(token)
            # print(user)
            
            # print(request.auth)
            # return Response({
            #         'token': token.key,
            #         'user_id': user.pk,
            #         'email': user.email
            #     })
            # user = authenticate(username=username, password=password)
            # print(request.user)
            # token = Token.objects.create(user=request.user)
            # if user is not None:
            #     print("authenticated")
            # else:
            #     print("not autheticated")


            # if request.user.is_authenticated:
            #     print("yes auth")
            # else:
            #     print("no auth")
                #logout(request) # ya lloguado

            # print(
            #     request.user.id
            # )


    if request.method == "POST":
        # print("post")
        query_me=schema.execute('''
        query{
            me{
                id
                email
            }
            }'''
            ,context_value=request
        )
        print("usuario:")
        print(query_me.data)
        print(query_me.errors)

        u = User.objects.get(id=pk)
        form = UpdateForm(request.POST, instance=u)
        if form.is_valid():
            print("form valid")
            isStaff=form.cleaned_data['is_staff']
            isStaff = json.dumps(isStaff)
            email=request.POST['email']
            firstName=request.POST['first_name']
            lastName=form.cleaned_data['last_name']
            image=form.data.get('image')
            # #avatar=request.POST['avatar']
            if request.FILES:
                # print("files")
                avatar=""
            else:
                avatar=image

            # update_string='''
            # mutation{{
            #     updateUser(email:"{abe}",isStaff:{b}, firstName:"{c}", id:{d}){{
            #         user{{
            #         email:email
            #         isStaff:isStaff
            #         firstName:firstName
            #         }}
            #     }}
            #     }}
            # '''.format(abe=email,b=isStaff,c=firstName,d=pk)

            #********** variables
            result=schema.execute(
                '''
                mutation updateUser ($id:ID!, $email:String!, $isStaff:Boolean, $firstName:String, $lastName:String, $avatar:String){
                updateUser(id:$id, email:$email, isStaff:$isStaff, firstName:$firstName, lastName:$lastName, avatar:$avatar ){
                    user{
                        email:email
                        isStaff:isStaff
                        firstName:firstName
                        lastName:lastName
                        avatar:avatar
                    }
                }
                }
                ''', variables={
                        "id":pk,
                        "email":email,
                        "isStaff":isStaff,
                        "firstName":firstName,
                        "lastName":lastName,
                        "avatar":avatar,
                        },
                        context_value=request
            )
            #******************
            #result=schema.execute(update_string)
            context={'form': form}
            # print(result.data)
            return render(request, 'edit.html', context)
        else:
            form = UpdateForm(instance=u)
            context={'form':form,
            'error':'NOT updated'
            }
            return render(request, 'edit.html', context)
    else:

        # quer_string='''
        #         query{{
        #         user(id:{id}){{
        #             email
        #             firstName
        #             isStaff
        #         }}
        #         }}

        # '''.format(id=pk)
        # result=schema.execute(quer_string)

        # ******* variable
        result= schema.execute(
            '''
                query getUser($id: Int) {
                user(id: $id) {
                id
                firstName
                lastName
                avatar
                email
                isStaff
                dateJoined
                }
            }
            '''
        , variables = {
            "id":pk
            },
            context=request
        )
        # **************

        print("not post")
        data=json.dumps(result.data )
        # print(data)
        form = UpdateForm(data=result.data)

        context={'form':form}
        return render(request, 'edit.html', context)


    # def post(self, request):
    #     return self.logout(request)

    # def logout(self, request):
    #     try:
    #         request.user.auth_token.delete()
    #     except (AttributeError, ObjectDoesNotExist):
    #         pass

    #     logout(request)

    #     return Response({"success": _("Successfully logged out.")},
    #                     status=status.HTTP_200_OK)
from django import forms
from crud.models import User, Mensaje


from django.forms import ModelForm, Textarea

class MensajeForm(forms.ModelForm):
    class Meta:
        model=Mensaje
        fields=['title','text','receiver']
        widgets={
            'text':Textarea()
            # 'receiver':
        }

class SingInForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    class Meta:
            model=User
            fields = ['email']

class SignUpForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput())
    password_again = forms.CharField(widget=forms.PasswordInput())

    def clean_email(self):
        value = self.cleaned_data['email'].strip()
        if User.objects.filter(email=value):
            raise forms.ValidationError("That email is already taken")
        return value

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data['password'] != cleaned_data['password_again']:
            raise forms.ValidationError("The passwords you entered did not match")

class UsersForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput())
    first_name=forms.CharField()

class UpdateForm(forms.ModelForm):
    image = forms.CharField(label='Imagen', widget=forms.HiddenInput, required=False) 
    
    class Meta:
        model = User
        fields = ['email', 'first_name',  'last_name','is_staff','avatar']
        widgets = {
            'avatar': forms.FileInput()
        }
    initial_fields = ['email', 'first_name',  'last_name','is_staff','avatar','image']
    def __init__(self, *args, **kwargs):
        data= kwargs.pop('data',None)

        super(UpdateForm, self).__init__(*args, **kwargs)
        if data:
            # print(data)

            self.fields['email'].initial = data['user']['email']
            self.fields['first_name'].initial=data['user']['firstName']
            self.fields['last_name'].initial=data['user']['lastName']
            self.fields['avatar'].initial=data['user']['avatar']
            self.fields['is_staff'].initial=data['user']['isStaff']
            self.fields['image'].initial=data['user']['avatar']
            
            # print(self.fields['image'].initial)

            for key in self.initial_fields:
                self.initial[key] = self.fields[key].initial 
            # print(self.initial)
            kwargs['initial']=self.initial
            super(UpdateForm, self).__init__(*args, **kwargs)


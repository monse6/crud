import graphene
import user.schema.schema
import mensaje.schema.schema

# import graphql_jwt
# from graphene_django.views import GraphQLView
# from graphql_jwt.decorators import jwt_cookie
# from graphql_jwt.decorators import login_required
# import graphql_jwt
# class Query(ingredients.schema.Query, graphene.ObjectType):
#     pass
# class Mutation(ingredients.schema.Mutation, graphene.ObjectType):
#     pass

class Query(
            mensaje.schema.schema.Query,
            user.schema.schema.Query,
            graphene.ObjectType):

    pass

class Mutation(
            mensaje.schema.schema.Mutation,
            user.schema.schema.Mutation,
            graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)